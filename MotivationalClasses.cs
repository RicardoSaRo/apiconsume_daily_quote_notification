﻿using System;

/// <summary>
/// MotivationalClasses is used to store the consumed data from the api
/// </summary>
public class MotivationalClasses
{
	public MotivationalClasses()
	{
        /// <summary>
        /// This class was obtained by converting the json file optained using postman to csharp in https://json2csharp.com/
        /// </summary>
        public class Root
        {
            public Root(string text, string author)
            {
                this.text = text;
                this.author = author;
            }

            public string text { get; set; }
            public string author { get; set; }
        }
    }
}
