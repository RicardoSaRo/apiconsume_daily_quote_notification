﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuotesDaily_SystemTray.Models
{
    /// <summary>
    /// BibleClasses is used to store the consumed data from the api
    /// </summary>
    public class BibleClasses
    {

        public class Data
        {
            public List<Passage>? passages { get; set; }
        }

        public class Meta
        {
            public string? fums { get; set; }
            public string? fumsId { get; set; }
            public string? fumsJsInclude { get; set; }
            public string? fumsJs { get; set; }
            public string? fumsNoScript { get; set; }
        }

        public class Passage
        {
            public Passage(string id, string orgId, string bibleId, string bookId, List<string> chapterIds, string reference, string content, int verseCount, string copyright)
            {
                this.id = id;
                this.orgId = orgId;
                this.bibleId = bibleId;
                this.bookId = bookId;
                this.chapterIds = chapterIds;
                this.reference = reference;
                this.content = content;
                this.verseCount = verseCount;
                this.copyright = copyright;
            }

            public string id { get; set; }
            public string orgId { get; set; }
            public string bibleId { get; set; }
            public string bookId { get; set; }
            public List<string> chapterIds { get; set; }
            public string reference { get; set; }
            public string content { get; set; }
            public int verseCount { get; set; }
            public string copyright { get; set; }
        }

        // Root myDeserializedClass = JsonConvert.DeserializeObject<Root>(myJsonResponse);
        public class Root
        {
            public Data? data { get; set; }
            public Meta? meta { get; set; }
        }
    }
}
