﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuotesDaily_SystemTray.Models
{
    /// <summary>
    /// Root is used to store the consumed data from the api
    /// </summary>
    public class Root
    {
        public Root(string text, string author)
        {
            this.text = text;
            this.author = author;
        }

        public string text { get; set; }
        public string author { get; set; }
    }
}
