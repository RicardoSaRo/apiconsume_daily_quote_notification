﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuotesDaily_SystemTray.Resources.Bible
{
    /// <summary>
    /// This classes are used to store the bible api codes and make a cleaner code to read in "GET method".
    /// </summary>
    public class Bible
    {
        public string ReinaValera1909 { get; } = "592420522e16049f-01";

        public string KJV_CambridgeParagraph { get; } = "55212e3cf5d04d49-01";
    }

    public class Verse
    {
        public string[] Verses { get; } = { "JER.29.11", "1COR.4.4", "PSA.23.1", "PHP.4.13", "JHN.3.16", "ROM.8.28",
            "ISA.41.10", "PSA.46.1", "GAL.5.22", "HEB.11.1", "1COR.10.13", "PRO.22.6", "ISA.40.31", "JOS.1.9", "HEB.12.2",
            "MAT.11.28", "ROM.10.9", "PHP.2.3", "MAT.5.43"};
        
    }
}
