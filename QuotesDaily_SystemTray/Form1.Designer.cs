﻿namespace QuotesDaily_SystemTray
{
    partial class Form1
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.rtxQuote = new System.Windows.Forms.RichTextBox();
            this.btnNewQuote = new System.Windows.Forms.Button();
            this.chkAlways = new System.Windows.Forms.CheckBox();
            this.cmbQuoteType = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.cmbBibleType = new System.Windows.Forms.ComboBox();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.showToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.closeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.notifyIcon1 = new System.Windows.Forms.NotifyIcon(this.components);
            this.contextMenuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // rtxQuote
            // 
            this.rtxQuote.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(166)))), ((int)(((byte)(206)))));
            this.rtxQuote.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.rtxQuote.Font = new System.Drawing.Font("Pristina", 20F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.rtxQuote.Location = new System.Drawing.Point(21, 20);
            this.rtxQuote.Name = "rtxQuote";
            this.rtxQuote.ReadOnly = true;
            this.rtxQuote.Size = new System.Drawing.Size(374, 179);
            this.rtxQuote.TabIndex = 1;
            this.rtxQuote.Text = "\"Daily Quote\"";
            // 
            // btnNewQuote
            // 
            this.btnNewQuote.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnNewQuote.FlatAppearance.BorderColor = System.Drawing.Color.RoyalBlue;
            this.btnNewQuote.Font = new System.Drawing.Font("News706 BT", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.btnNewQuote.Location = new System.Drawing.Point(256, 264);
            this.btnNewQuote.Name = "btnNewQuote";
            this.btnNewQuote.Size = new System.Drawing.Size(128, 41);
            this.btnNewQuote.TabIndex = 2;
            this.btnNewQuote.Text = "New Quote";
            this.btnNewQuote.UseVisualStyleBackColor = true;
            this.btnNewQuote.Click += new System.EventHandler(this.btnNewQuote_Click);
            // 
            // chkAlways
            // 
            this.chkAlways.AutoSize = true;
            this.chkAlways.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.chkAlways.Location = new System.Drawing.Point(235, 205);
            this.chkAlways.Name = "chkAlways";
            this.chkAlways.Size = new System.Drawing.Size(165, 19);
            this.chkAlways.TabIndex = 3;
            this.chkAlways.Text = "Display this Quote always";
            this.chkAlways.UseVisualStyleBackColor = true;
            this.chkAlways.CheckedChanged += new System.EventHandler(this.chkAlways_CheckedChanged);
            // 
            // cmbQuoteType
            // 
            this.cmbQuoteType.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.cmbQuoteType.Font = new System.Drawing.Font("News706 BT", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.cmbQuoteType.FormattingEnabled = true;
            this.cmbQuoteType.Items.AddRange(new object[] {
            "Motivational",
            "Biblical",
            "Famous"});
            this.cmbQuoteType.Location = new System.Drawing.Point(30, 273);
            this.cmbQuoteType.Name = "cmbQuoteType";
            this.cmbQuoteType.Size = new System.Drawing.Size(204, 27);
            this.cmbQuoteType.TabIndex = 4;
            this.cmbQuoteType.SelectedIndexChanged += new System.EventHandler(this.cmbQuoteType_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.label1.Location = new System.Drawing.Point(30, 249);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(169, 21);
            this.label1.TabIndex = 5;
            this.label1.Text = "Current Quote Types:";
            // 
            // cmbBibleType
            // 
            this.cmbBibleType.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.cmbBibleType.Font = new System.Drawing.Font("News706 BT", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.cmbBibleType.FormattingEnabled = true;
            this.cmbBibleType.Items.AddRange(new object[] {
            "Reina Valera 1909",
            "KJV - Stanford Paragraph"});
            this.cmbBibleType.Location = new System.Drawing.Point(30, 306);
            this.cmbBibleType.Name = "cmbBibleType";
            this.cmbBibleType.Size = new System.Drawing.Size(204, 27);
            this.cmbBibleType.TabIndex = 4;
            this.cmbBibleType.SelectedIndexChanged += new System.EventHandler(this.cmbBibleType_SelectedIndexChanged);
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.showToolStripMenuItem,
            this.closeToolStripMenuItem});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(104, 48);
            // 
            // showToolStripMenuItem
            // 
            this.showToolStripMenuItem.Name = "showToolStripMenuItem";
            this.showToolStripMenuItem.Size = new System.Drawing.Size(103, 22);
            this.showToolStripMenuItem.Text = "Show";
            this.showToolStripMenuItem.Click += new System.EventHandler(this.showToolStripMenuItem_Click);
            // 
            // closeToolStripMenuItem
            // 
            this.closeToolStripMenuItem.Name = "closeToolStripMenuItem";
            this.closeToolStripMenuItem.Size = new System.Drawing.Size(103, 22);
            this.closeToolStripMenuItem.Text = "Close";
            this.closeToolStripMenuItem.Click += new System.EventHandler(this.closeToolStripMenuItem_Click);
            // 
            // notifyIcon1
            // 
            this.notifyIcon1.ContextMenuStrip = this.contextMenuStrip1;
            this.notifyIcon1.Icon = ((System.Drawing.Icon)(resources.GetObject("notifyIcon1.Icon")));
            this.notifyIcon1.Text = "Daily Quotes";
            this.notifyIcon1.Visible = true;
            this.notifyIcon1.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.notifyIcon1_MouseDoubleClick);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(166)))), ((int)(((byte)(206)))));
            this.ClientSize = new System.Drawing.Size(418, 352);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.cmbBibleType);
            this.Controls.Add(this.cmbQuoteType);
            this.Controls.Add(this.chkAlways);
            this.Controls.Add(this.btnNewQuote);
            this.Controls.Add(this.rtxQuote);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "DAILY QUOTES";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
            this.Load += new System.EventHandler(this.Form1_Load);
            this.Shown += new System.EventHandler(this.Form1_Shown);
            this.Move += new System.EventHandler(this.Form1_Move);
            this.Resize += new System.EventHandler(this.Form1_Resize);
            this.contextMenuStrip1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private RichTextBox rtxQuote;
        private Button btnNewQuote;
        private CheckBox chkAlways;
        private ComboBox cmbQuoteType;
        private Label label1;
        private ComboBox cmbBibleType;
        private ContextMenuStrip contextMenuStrip1;
        private ToolStripMenuItem showToolStripMenuItem;
        private ToolStripMenuItem closeToolStripMenuItem;
        private NotifyIcon notifyIcon1;
    }
}