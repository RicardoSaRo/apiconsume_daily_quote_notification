using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using MotivRoot = QuotesDaily_SystemTray.Models.Root;
using BibleType = QuotesDaily_SystemTray.Resources.Bible.Bible;
using BibleRoot = QuotesDaily_SystemTray.Models.BibleClasses;
using Verse = QuotesDaily_SystemTray.Resources.Bible.Verse;
using QuotesDaily_SystemTray.Resources;

namespace QuotesDaily_SystemTray
{
    /// <summary>
    /// This is the main code of the project. Focuses in consuming the info needed from the APIs and
    /// making use of the Settings, save information for if the user wants a quote to be display at
    /// every startup.
    /// </summary>
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        //--> Here is saved the list of quotes consumed before selecting a random one to display.
        //--> The motivational API does not have an option to just consume one, so all have to be consumed.
        public List<MotivRoot>? MainQuoteList = new List<MotivRoot>();

        /// <summary>
        /// At form load, you have to consume the API. Tried to consume the API while minimized but does not work.
        /// Its important to say that the consumption must be done in a Task and be told to wait response, if not
        /// the consumption will not be stored in any varibles (because is not being in the same thread), not even
        /// in the settings.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Form1_Load(object sender, EventArgs e)
        {
            //--> Checks if the user wants a message to be showed always. Takes the index from the settings
            //--> to find the specific quote.
            if (Settings.Default.Always)
            {
                chkAlways.Checked = Settings.Default.Always;
                index = Settings.Default.Index;
            }
            //--> Runs the task to prevent thread problems after consuming
            Task.Run(() => ConsumeAPIS(Settings.Default.QuoteType)).Wait();
            string We = Settings.Default.TempQuote;
            Settings.Default.Save();
        }

        //--> RichTextBox Text Alignment
        public void CorrectAlignment()
        {
            rtxQuote.SelectAll();
            rtxQuote.SelectionAlignment = HorizontalAlignment.Center;
            rtxQuote.DeselectAll();
        }

        /// <summary>
        /// Gets the motivational quotes list in json format, its deserialized into a MotivRoot list
        /// to get a random quote from it.
        /// </summary>
        [HttpGet]
        public async void GetMotivationalQuotes()
        {
            HttpClient client = new HttpClient();
            HttpResponseMessage responseMessage = await client.GetAsync("https://type.fit/api/quotes");
            if (responseMessage.IsSuccessStatusCode)
            {
                var jsonMotivQuotes = await responseMessage.Content.ReadAsStringAsync();
                MainQuoteList = JsonConvert.DeserializeObject<List<MotivRoot>>(jsonMotivQuotes);
                MotivRoot Quote = GetRandomQuote(MainQuoteList);
                Settings.Default.TempQuote = '"' + Quote.text + '"' + "\n ~" + Quote.author;
            }
            else
            {
                Settings.Default.TempQuote = "Error. Cannot connect with the Server.";   
            }
        }

        /// <summary>
        /// Gets the motivational quotes list in json format, its deserialized into a BibleRoot list
        /// to get a random quote from it. API Key and Secret are not included to prevent use of my
        /// personal key and secret from fellow programmers looking at this in the public repository.
        /// </summary>
        [HttpGet]
        public async void GetBibleQuote()
        {
            using (HttpClient client = new HttpClient())
            {
                Verse bible = new Verse();
                BibleType bibletype = new BibleType();
                Random random = new Random();
                string bibleType = (Settings.Default.BibleType == 0) ? bibletype.ReinaValera1909 : bibletype.KJV_CambridgeParagraph;
                string verse = bible.Verses[random.Next(1,18)];
                string uri = "https://api.scripture.api.bible/v1/bibles/"+ bibleType +"/search?query="+verse;
                HttpRequestMessage requestMessage = new HttpRequestMessage();
                requestMessage.Method = HttpMethod.Get;
                requestMessage.RequestUri = new Uri(uri);
                requestMessage.Headers.Add("XXX", "XXXXXXXXXXXXXX");
                HttpResponseMessage responseMessage = await client.SendAsync(requestMessage);
                if (responseMessage.IsSuccessStatusCode)
                {
                    var jsonMotivQuotes = await responseMessage.Content.ReadAsStringAsync();
                    BibleRoot.Root Quote = JsonConvert.DeserializeObject<BibleRoot.Root>(jsonMotivQuotes)!;
                    var passage = Quote.data!.passages!.FirstOrDefault()!;
                    string content = passage.content;
                    Settings.Default.TempQuote = CleanPassage(passage);
                    Settings.Default.Save();
                }
                else
                {
                    Settings.Default.TempQuote = "Error. Cannot connect with the Server.";
                }
            }
        }

        /// <summary>
        /// The bible quote json is very dirty with xml/hmtl code. With this method cleans the string.
        /// I must say its very specific and focused to clean the passages included in Bible.cs...
        /// If more passages are included, its probable that new clean-up options have to be
        /// included in the replaceDic dictonary.
        /// </summary>
        /// <param name="passage"></param>
        /// <returns> A string without xml/html code </returns>
        public string CleanPassage(BibleRoot.Passage passage)
        {
            Dictionary<int, string> replaceDic = new Dictionary<int, string>()
            {
                {1, "</span>"}, {2, "<span class=\"it\">"}, {3,"</p>"}, {4,"<span class=\"add\">"}, {5,"<span>"},
                {6, "<span class=\"sc\">"}, {7, ",<p data-vid=\"PSA 46:1\"" }, {8,"<p data-vid=\"PRO 22:6\""},
                {9, "class=\"q1\">"}, {10,"class=\"q\">"}
};

            string inicialcut = passage.content.Substring(0,passage.content.IndexOf("</span>"));
            passage.content = passage.content.Replace(inicialcut, "");

            bool Ocurrence = true;

            while (Ocurrence)
            {
                Ocurrence = false;
                foreach (var item in replaceDic)
                {
                    if(passage.content.Contains(item.Value))
                    {
                        Ocurrence = true;
                        passage.content = passage.content.Replace(item.Value, "");
                    }
                }
            }

            return '"' + passage.content + '"' + "\n" + passage.reference;
        }

        /// <summary>
        /// Looks for a new quote when the New Quote button is clicked.
        /// This method runs the API consumption on a task and waits response to be
        /// sure that the consumption is stored properly.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnNewQuote_Click(object sender, EventArgs e)
        {
            Task.Run(() => ConsumeAPIS(Settings.Default.QuoteType)).Wait();
            rtxQuote.Text = Settings.Default.TempQuote;
            CorrectAlignment();
        }

        /// <summary>
        /// Consumes API depending on the quote type. If more types of quotes are included
        /// this method have to include them in a new "else if".
        /// </summary>
        /// <param name="QType"></param>
        public void ConsumeAPIS(int QType)
        {
            if (QType == 0) GetMotivationalQuotes();
            else if (QType == 1) GetBibleQuote();
            else GetMotivationalQuotes();
            Settings.Default.Save();

        }

        //--> Index in case the "Always" checkbox is checked
        public int index = 0;

        /// <summary>
        /// If Always checkbox is checked, the method will give you the same quote everytime. If not,
        /// the method will give you a random motivational quote from the list.
        /// </summary>
        /// <param name="list"></param>
        /// <returns> Fixed or random motivational quote </returns>
        private MotivRoot GetRandomQuote(List<MotivRoot>? list)
        {
            if (chkAlways.Checked)
            {
                return list![index];
            }
            else
            {
                Random rndmQuote = new Random();
                index = rndmQuote.Next(list!.Count);
                return list[index];
            }
        }

        /// <summary>
        /// When the Quote type combobox changes the index, stores new values into the settings.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmbQuoteType_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cmbQuoteType.SelectedIndex == 1) cmbBibleType.Visible = true;
            else cmbBibleType.Visible = false;
            if (cmbBibleType.SelectedIndex == -1) cmbBibleType.SelectedIndex = 0;
            Settings.Default.QuoteType = cmbQuoteType.SelectedIndex;
            Settings.Default.BibleType = cmbBibleType.SelectedIndex;
            Settings.Default.Save();
        }

        /// <summary>
        /// Stores in settings the Always checkbox changes
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void chkAlways_CheckedChanged(object sender, EventArgs e)
        {
            if (chkAlways.Checked)
            {
                Settings.Default.Index = index;
            }
            else
            {
                Settings.Default.Index = 0;
            }
            Settings.Default.Always = chkAlways.Checked;
            //Settings.Default.Save();
        }

        /// <summary>
        /// Stores changes in Settings of BibleType combobox
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmbBibleType_SelectedIndexChanged(object sender, EventArgs e)
        {
            Settings.Default.BibleType = cmbBibleType.SelectedIndex;
            //Settings.Default.Save();
        }

        //--> Show the application window when selected in the system tray
        private void showToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Show();
            WindowState = FormWindowState.Normal;
        }

        //--> Closes the application when selected in the system tray
        private void closeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Settings.Default.Save();
            Application.Exit();
        }

        private void Form1_Move(object sender, EventArgs e)
        {
            //--> Notting to see here... Made to test minimization behaviours.
        }

        //--> SHows the application window when the notification is double clicked
        private void notifyIcon1_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            this.Show();
        }

        //--> Hides the form when its minimized
        private void Form1_Resize(object sender, EventArgs e)
        {
            if (this.WindowState == FormWindowState.Minimized)
            {
                this.Hide();
            }
        }

        /// <summary>
        /// This event applies only when the form is first shown. Consumes the default API and shows the notification.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Form1_Shown(object sender, EventArgs e)
        {
            ConsumeAPIS(Settings.Default.QuoteType);
            cmbQuoteType.SelectedIndex = Settings.Default.QuoteType;
            cmbBibleType.SelectedIndex = Settings.Default.BibleType;
            if (Settings.Default.QuoteType == 0) cmbBibleType.Visible = false;
            this.WindowState = FormWindowState.Minimized;
            notifyIcon1.ShowBalloonTip(5000, "Daily Quote", Settings.Default.TempQuote, ToolTipIcon.Info);
            rtxQuote.Text = Settings.Default.TempQuote;
            CorrectAlignment();
        }

        //--> Saves Settings when the application is closed
        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            Settings.Default.Save();
        }
    }
}