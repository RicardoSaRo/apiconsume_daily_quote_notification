﻿using System;

/// <summary>
/// BibleClasses is used to store the consumed data from the api
/// </summary>
public class BibleClasses
{
	public BibleClasses()
	{
        /// <summary>
        /// This class was obtained by converting the json file optained using postman to csharp in https://json2csharp.com/
        /// </summary>
        // Root myDeserializedClass = JsonConvert.DeserializeObject<Root>(myJsonResponse);
        public class Data
        {
            public List<Passage> passages { get; set; }
        }

        public class Meta
        {
            public string fums { get; set; }
            public string fumsId { get; set; }
            public string fumsJsInclude { get; set; }
            public string fumsJs { get; set; }
            public string fumsNoScript { get; set; }
        }

        public class Passage
        {
            public string id { get; set; }
            public string orgId { get; set; }
            public string bibleId { get; set; }
            public string bookId { get; set; }
            public List<string> chapterIds { get; set; }
            public string reference { get; set; }
            public string content { get; set; }
            public int verseCount { get; set; }
            public string copyright { get; set; }
        }

        public class Root
        {
            public Data data { get; set; }
            public Meta meta { get; set; }
        }
    }
}
